﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWheels : MonoBehaviour
{
    [SerializeField] private Rigidbody rbRef;
    private Vector3 eulerRot = Vector3.zero;
    public float radius;
    private float circumference;

    void Start()
    {
        circumference = radius * 2 * Mathf.PI;
    }

    void Update()
    {
        if (rbRef)
        {
            float distance = 0f;
            if (!rbRef.constraints.HasFlag(RigidbodyConstraints.FreezePositionZ))
                distance = rbRef.velocity.z * Time.deltaTime;
            else if (!rbRef.constraints.HasFlag(RigidbodyConstraints.FreezePositionX))
                distance = rbRef.velocity.x * Time.deltaTime;
            
            eulerRot.x = (distance / circumference) * (2 * Mathf.PI) * Mathf.Rad2Deg;
            transform.Rotate(eulerRot);
        }
    }
}
