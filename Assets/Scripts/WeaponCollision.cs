﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCollision : MonoBehaviour
{
    [SerializeField] private Transform attackOrigin;
    [SerializeField] private StasisData data;
    [SerializeField] private bool overrideDamage = false;
    [SerializeField] private float OverridenForcePerHit = 50;

    private void OnTriggerEnter(Collider other)
    {
        StasisObject so = other.gameObject.GetComponent<StasisObject>();
        if (so != null)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/StasisHit", transform.position);
            if (overrideDamage)
            {
                so.AddStasisForce(OverridenForcePerHit, attackOrigin.position);
            }
            else if (!overrideDamage)
            {
                so.AddStasisForce(data.forcePerHit, attackOrigin.position);
            }
        }
    }
}
