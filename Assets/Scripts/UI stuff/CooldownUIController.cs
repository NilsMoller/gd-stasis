﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooldownUIController : MonoBehaviour
{
    [SerializeField] private float TotalCooldown = 3;
    [HideInInspector] private float CurrentCooldown;
    [SerializeField] private Image icon;
    [SerializeField] UnityEventScriptableObject OnStasisEnd;
    [SerializeField] UnityEventScriptableObject OnCooldownFinished;
    [SerializeField] UnityEventScriptableObject OnPlayerDied;

    private void Start()
    {
        CurrentCooldown += TotalCooldown;
        OnStasisEnd.Event.AddListener(StasisEnded);
        OnPlayerDied.Event.AddListener(PlayerDied);
    }
    private void Update()
    {
        if (CurrentCooldown < TotalCooldown)
        {
            CurrentCooldown += Time.deltaTime;
            icon.fillAmount = 1 - CurrentCooldown / TotalCooldown;
            if (CurrentCooldown >= TotalCooldown)
            {
                OnCooldownFinished.Event.Invoke();
            }
        }
    }
    
    private void StasisEnded()
    {
        CurrentCooldown = 0;
    }

    private void PlayerDied()
    {
        gameObject.SetActive(false);
    }
}
