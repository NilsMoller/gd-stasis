﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBenRotations : MonoBehaviour
{
    [SerializeField] private Transform arrowHour;
    [SerializeField] private Transform arrowMinute;
    [SerializeField] private Transform winder;

    [SerializeField] private float winderDPS;
    [SerializeField] private float minuteSpeed;

    void FixedUpdate()
    {
        //arrowHour.localEulerAngles = Vector3.forward * System.DateTime.Now.Hour * 30;
        //arrowMinute.localEulerAngles = Vector3.forward * System.DateTime.Now.Minute * 6;
        arrowMinute.Rotate(Vector3.forward * minuteSpeed * Time.deltaTime);
        arrowHour.Rotate(Vector3.forward * (minuteSpeed / 12) * Time.deltaTime);
        winder.Rotate(Vector3.forward * winderDPS * Time.deltaTime);
    }
}
