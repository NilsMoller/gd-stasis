﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRoomEnter : MonoBehaviour
{
    [SerializeField] GameObject player;
    [SerializeField] GameObject boss;
    [SerializeField] GameObject SpawnRoom;
    [SerializeField] SecurityNetworkData BossNetwork;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            SpawnRoom.SetActive(false);
            boss.GetComponent<BossController>().Aggro();
        }
    }
}
