﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    [SerializeField] private GameObject Player;
    [SerializeField] private int HP = 3;
    [SerializeField] private SecurityNetworkData network;
    [SerializeField] private GameObject FinishRoom;

    private EnemyTurret[] Turrets;
    [SerializeField] private StasisBallLauncher[] BallLaunchers;
    [SerializeField] private StasisCartLauncher[] CartLaunchers;

    [SerializeField] float FireIntervalPhase2 = 2f;
    [SerializeField] float FireIntervalPhase3 = 1f;
    [SerializeField] float BaseLaunchForce = 50f;
    [SerializeField] float DieForceToAdd = 50f;

    private bool IsDead = false;

    void Start()
    {
        FinishRoom.SetActive(false);

        Turrets = GetComponentsInChildren<EnemyTurret>();
        
        DisableBallShooting();
        DisableTurretShooting();
        DisableMinecartRacing();
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsDead)
        {
            transform.LookAt(network.targetTransform);
        }
    }

    public void Aggro()
    {
        network.targetTransform = Player.transform;
        network.IsPlayerDetected = true;
        FMODUnity.RuntimeManager.PlayOneShot("event:/BossStateChange");
        foreach (StasisBallLauncher ballLauncher in BallLaunchers)
        {
            ballLauncher.LaunchForce = BaseLaunchForce;
        }
        EnableBallShooting();
        DisableTurretShooting();
        DisableMinecartRacing();

        foreach (StasisBallLauncher ballLauncher in BallLaunchers)
        {
            ballLauncher.StartSpawningBalls();
        }
    }


    public void TakeDamage()
    {
        HP -= 1;
        Debug.Log($"HP is now {HP}");

        if (HP == 2)
        {
            //play sound
            FMODUnity.RuntimeManager.PlayOneShot("event:/BossStateChange");
            //activate new bahaviour
            EnableBallShooting();
            foreach (StasisBallLauncher ballLauncher in BallLaunchers)
            {
                ballLauncher.spawnDelay -= 1f;
            }
            EnableTurretShooting();
            foreach (EnemyTurret turret in Turrets)
            {
                turret.fireInterval = FireIntervalPhase2;
            }
            DisableMinecartRacing();
        }
        else if (HP == 1)
        {
            //play sound
            FMODUnity.RuntimeManager.PlayOneShot("event:/BossStateChange");
            //activate new bahaviour
            EnableBallShooting();
            foreach (StasisBallLauncher ballLauncher in BallLaunchers)
            {
                ballLauncher.spawnDelay -= 1f;
            }
            EnableTurretShooting();
            foreach (EnemyTurret turret in Turrets)
            {
                turret.fireInterval = FireIntervalPhase3;
            }
            EnableMinecartRacing();
            foreach (StasisCartLauncher cartLauncher in CartLaunchers)
            {
                cartLauncher.SpawnNewObject();
            }

        }
        else if (HP == 0)
        {
            //play sound
            FMODUnity.RuntimeManager.PlayOneShot("event:/BossDeath");
            //activate new bahaviour
            DisableBallShooting();
            DisableTurretShooting();
            DisableMinecartRacing();

            FinishRoom.SetActive(true);
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.isKinematic = false;
            rb.AddExplosionForce(DieForceToAdd, transform.position, 1);
            IsDead = true;
        }
    }

    void EnableBallShooting()
    {
        foreach (StasisBallLauncher ballLauncher in BallLaunchers)
        {
            ballLauncher.enabled = true;
        }
        Debug.Log("Enabled ball shooting");
    }
    void DisableBallShooting()
    {
        foreach (StasisBallLauncher ballLauncher in BallLaunchers)
        {
            ballLauncher.enabled = false;
        }
        Debug.Log("disabled ball shooting");
    }
    void EnableTurretShooting()
    {
        Renderer rend = GetComponent<Renderer>();
        foreach (EnemyTurret turret in Turrets)
        {
            turret.enabled = true;
        }
        
        Debug.Log("Enabled turrets");
    }
    void DisableTurretShooting()
    {
        foreach (EnemyTurret turret in Turrets)
        {
            turret.enabled = false;
        }
        Debug.Log("turrets disabled");
    }
    void EnableMinecartRacing()
    {
        foreach (StasisCartLauncher cartLauncher in CartLaunchers)
        {
            cartLauncher.enabled = true;
        }
        Debug.Log("turned on carts");
    }
    void DisableMinecartRacing()
    {
        foreach (StasisCartLauncher cartLauncher in CartLaunchers)
        {
            cartLauncher.enabled = false;
        }
        Debug.Log("turned off carts");
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("StasisObject"))
        {
            TakeDamage();
        }
    }
}
