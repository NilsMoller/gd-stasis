﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StasisController : MonoBehaviour
{
    [SerializeField] private StasisObjectRuntimeSet sors;
    [SerializeField] private UnityEventScriptableObject OnStasisActivate;
    [SerializeField] private UnityEventScriptableObject OnCooldownFinished;
    [HideInInspector] private Transform cameraT;
    [HideInInspector] private bool isOnCooldown = false;


    private void Start()
    {
        cameraT = Camera.main.transform;
        OnCooldownFinished.Event.AddListener(CooldownFinished);
    }
    public bool UpdateStasis(bool activate)
    {
        RaycastHit hit;
        if (Physics.Raycast(cameraT.position, cameraT.forward, out hit, float.MaxValue)) // Sends ray from camera position to check if looking at StasisObject.
        {
            StasisObject so = hit.transform.GetComponent<StasisObject>();
            if (so != null)
            {
                if (activate && !isOnCooldown)
                {
                    so.ToggleStasis(); // If wants to "attack" put in stasis
                    OnStasisActivate.Event.Invoke();
                    isOnCooldown = true;
                    return true;
                }
                else
                {
                    sors.SetHover(so); // Else enable hovering
                    return false;
                }
            }
        }

        sors.SetHover(); // SetHover with no value resets all objects
        return false;
    }

    private void CooldownFinished()
    {
        isOnCooldown = false;
    }

}
