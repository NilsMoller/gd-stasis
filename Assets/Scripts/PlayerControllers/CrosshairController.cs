﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrosshairController : MonoBehaviour
{
    [SerializeField] UnityEventScriptableObject OnPlayerDied;

    private void Start()
    {
        OnPlayerDied.Event.AddListener(PlayerDied);
    }

    private void PlayerDied()
    {
        gameObject.SetActive(false);
    }
}
