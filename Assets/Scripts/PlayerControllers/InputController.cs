﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [HideInInspector] public bool AllowInput = true;
    MovementController mc;
    StasisController sc;

    void Start()
    {
        mc = GetComponent<MovementController>();
        sc = GetComponent<StasisController>();
    }

    void Update()
    {
        if (AllowInput)
        {
            mc.inputDir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")).normalized;
            mc.wantsToRun = Input.GetKey(KeyCode.LeftShift);

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                mc.Attack();
            }
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                sc.UpdateStasis(true);
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                mc.Jump();
            }
        }
    }
}
